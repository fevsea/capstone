# Overview
This project aims to predict the fare of a taxi trip considering only information one can have beforehand (origin, destination, date and time)

# Motivation
The final cost of a taxi trip is usually a surprise, depending on numerous factors that can not be foreseen in advance. Although the most important are the time and the distance of the route, there are many others that affect in a more indirect way, such as the traffic of a determined area, the weather, the time of the day...

In this project I have the objective of predicting the final cost of a trip considering only information you can have beforehand. To fit the project into specific data, I'll use the Chicago City rides of May 2016, on a supervised learning regression setting.

# How to run the code
There are two Juyter notebooks on the 'notebook' folder containing the code.

There are two notebooks:

1. data_preprocess.ipynb -&gt; Data cleaning and exploration
2. regression.ipynb -&gt; Model building and analysis

To run them execute this command on the 'notebook' folder.
'''bash
jupyter notebook .
'''

# Requirements

This project is heavily based on supervised reinforcement learning.

## Libraries

In order to run the code some python libraries need to be installed, which are listed on 'requirements.txt'
The principal libraries are `pandas` (data manipulation),  `seaborn` (data visualization), scikit-learn (model building).

To install them:

'''bash
pip install -r requirements.txt
'''

## Data
In order to avoid uploading large files to the repository, in order to run the data_preprocess notebook the dataset for the month of may have to be downloaded into the data folder without changing the name. It can be downloaded from kaggle:

https://www.kaggle.com/chicago/chicago-taxi-rides-2016

Cleaned data is included on the repo, so `regression.ipynb` can be run withouth downloading any dataset.